package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomeController implements SomeApi {
    @Override
    @PreAuthorize("hasAuthorityAtLeast('GreatSecurity')")
    public ResponseEntity<String> hello(String name) {
        return ResponseEntity.ok(String.format("Hello %s", name));
    }

    @RequestMapping(value = "/exception",
            method = RequestMethod.GET)
    @PreAuthorize("hasAuthorityAtLeast('GreatSecurity')")
    public ResponseEntity<String> exception(String name) {
        throw new RuntimeException("message");
    }
}
