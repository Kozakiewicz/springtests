package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface SomeApi {

    @RequestMapping(value = "/hello/{name}",
            method = RequestMethod.GET)
    ResponseEntity<String> hello(@PathVariable("name") String name);
}
