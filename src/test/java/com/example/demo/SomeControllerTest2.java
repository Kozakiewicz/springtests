package com.example.demo;

import a.b.c.CommonExceptionHandlerAdvice;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@WebMvcTest(controllers = {SomeController.class})
@ContextConfiguration(classes = {SomeController.class, CommonExceptionHandlerAdvice.class})
class SomeControllerTest2 {

    @Autowired
    protected WebApplicationContext wac;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        this.mockMvc = webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void helloUnauthorized() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = "InsuffiecientRights")
    void helloForbidden() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void hello() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(content().string("Hello ziom"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "GreatSecurity")
    void exception() throws Exception {
        mockMvc.perform(get("/exception"))
                .andExpect(content().string("message"))
                .andExpect(status().isNotFound());
    }
}