package com.example.demo;

import a.b.c.CommonExceptionHandlerAdvice;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {SomeController.class})
@ContextConfiguration(classes = {SomeController.class, CommonExceptionHandlerAdvice.class})
class SomeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void helloUnauthorized() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = "InsuffiecientRights")
    void helloForbidden() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void hello() throws Exception {
        mockMvc.perform(get("/hello/ziom"))
                .andExpect(content().string("Hello ziom"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "GreatSecurity")
    void exception() throws Exception {
        mockMvc.perform(get("/exception"))
                .andExpect(content().string("message"))
                .andExpect(status().isNotFound());
    }
}